import React from "react";
import "./App.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import CategoryList from "./Try/CategoryList";
import GraphQl from "./Try/GraphQl";

function App() {
  return (
    <div className="app">
      <h1>React Hook - GraphQl</h1>
      <GraphQl />
    </div>
  );
}

export default App;
