import React, { useState, useEffect } from "react";
import { ListGroup, ListGroupItem } from "reactstrap";
import gpl from "graphql-tag";
import { graphql } from "react-apollo";

const AppQuery = gpl`
  query {
    animals {
      id,
      name
    }
  }
`;

const GraphQl = (props) => {
  const [animals, setAnimals] = useState([]);

  useEffect(() => {
    if (props.data.animals) {
      setAnimals(props.data.animals);
    }
  }, [props]);

  return (
    <ListGroup>
      {animals.map((item) => (
        <ListGroupItem key={item.id}>{item.name}</ListGroupItem>
      ))}
    </ListGroup>
  );
};

const AppWithData = graphql(AppQuery)(GraphQl);

export default AppWithData;
