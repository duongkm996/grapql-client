import React, { useState } from "react";
import "./ColorBox.scss";

const getRandomColor = () => {
  const COLOR_LIST = ["blue", "green", "red", "yellow", "deeppink"];
  const RADOM_COLOR_INDEX = Math.trunc(Math.random() * 5);
  return COLOR_LIST[RADOM_COLOR_INDEX];
};

const ColorBox = () => {
  const [color, setColor] = useState(() => {
    const initColor = localStorage.getItem("box_color") || "deeppink";
    console.log(initColor);
    return initColor;
  });

  function handleBoxClick() {
    const newColor = getRandomColor();
    setColor(newColor);
    localStorage.setItem("box_color", newColor);
  }

  return (
    <div
      className="color-box"
      style={{ backgroundColor: color }}
      onClick={handleBoxClick}
    >
      Color Box
    </div>
  );
};

export default ColorBox;
