import React, { useState } from "react";
import { Form, FormGroup, Label, Input } from "reactstrap";

const FormTodo = (props) => {
  const { handleSubmitForm } = props;
  const [value, setValue] = useState("");

  function handleChange(e) {
    setValue(e.target.value);
  }

  function handleSubmit(e) {
    e.preventDefault();
    const formData = {
      title: value,
    };
    handleSubmitForm(formData);
    setValue("");
  }

  return (
    <Form onSubmit={handleSubmit}>
      <FormGroup>
        <Label for="exampleEmail">Add Todo</Label>
        <Input type="text" value={value} name="text" onChange={handleChange} />
      </FormGroup>
    </Form>
  );
};

export default FormTodo;
