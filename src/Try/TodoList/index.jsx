import React, { useState } from "react";
import Items from "./items";
import FormTodo from "./form";

const TodoList = () => {
  const [todoList, setTodoLIst] = useState(() => {
    const initTodoList = [
      {
        id: 1,
        title: "Học React Hook",
      },
      {
        id: 2,
        title: "Học GraphQL Client",
      },
    ];
    return initTodoList;
  });

  function handleRemove(item) {
    // const id = e.target.dataset.id;
    const id = item.id;
    const index = todoList.findIndex((elm) => elm.id == id);
    if (index < 0) return;
    const newTodolist = [...todoList];
    newTodolist.splice(index, 1);
    setTodoLIst(newTodolist);
  }

  function handleSubmitForm(formData) {
    const newTodoList = [...todoList];
    newTodoList.push({
      id: todoList.length + 1,
      ...formData,
    });
    setTodoLIst(newTodoList);
  }

  return (
    <>
      <FormTodo handleSubmitForm={handleSubmitForm} />
      <Items listItems={todoList} handleRemove={handleRemove} />
    </>
  );
};

export default TodoList;
