import React from "react";
import { ListGroup, ListGroupItem } from "reactstrap";

const Items = (props) => {
  const { listItems, handleRemove } = props;
  const items = listItems.map((item) => (
    <ListGroupItem
      data-id={item.id}
      onClick={() => handleRemove(item)}
      key={item.id}
    >
      {item.title}
    </ListGroupItem>
  ));
  return (
    <>
      <ListGroup>{items}</ListGroup>
    </>
  );
};

export default Items;
