import React, { useState, useEffect, useRef } from "react";
import {
  ListGroup,
  ListGroupItem,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";

const queryString = require("query-string");

const CategoryList = (props) => {
  const typingTimeoutRef = useRef(null);

  const [categories, setCategories] = useState([]);

  const [paginate, setPaginate] = useState({
    current_page: null,
    total_page: null,
    per_page: null,
    total: null,
  });

  const [filter, setFilter] = useState({
    page: 1,
    limit: 5,
    search: "",
  });

  const [search, setSearch] = useState("");

  useEffect(() => {
    loadCategory();
  }, [filter]);

  function handleChangePage(newPage) {
    setFilter({
      ...filter,
      page: newPage,
    });
  }

  async function loadCategory() {
    const paramString = queryString.stringify(filter);
    const requestUrl = `http://totoshop.test/api/article-category?${paramString}`;
    try {
      const response = await window.axios.get(requestUrl);
      const data = response.data;
      const list = data.data;
      const { paginate } = data;
      setCategories(list);
      setPaginate(paginate);
    } catch (error) {
      console.log(error);
    }
  }

  function handleChangeSearch(e) {
    const value = e.target.value;
    setSearch(value);

    if (typingTimeoutRef.current) {
      clearTimeout(typingTimeoutRef.current);
    }

    typingTimeoutRef.current = setTimeout(() => {
      setFilter({
        ...filter,
        search: value,
      });
    }, 300);
  }

  return (
    <>
      <Form>
        <FormGroup>
          <Label for="exampleEmail">Search</Label>
          <Input
            type="text"
            name="search"
            value={search}
            onChange={handleChangeSearch}
          />
        </FormGroup>
      </Form>
      {categories.length ? (
        <div>
          <ListGroup>
            {categories.map((category) => (
              <ListGroupItem key={category.id}>{category.name}</ListGroupItem>
            ))}
          </ListGroup>
          <Button
            color="primary"
            className="mr-3 mt-3"
            disabled={paginate.current_page <= 1 ? true : false}
            onClick={() => handleChangePage(paginate.current_page - 1)}
          >
            Prev
          </Button>
          <Button
            color="primary"
            className="mt-3"
            disabled={
              paginate.current_page >= paginate.total_page ? true : false
            }
            onClick={() => handleChangePage(paginate.current_page + 1)}
          >
            Next
          </Button>
        </div>
      ) : (
        <div>...Loading</div>
      )}
    </>
  );
};

export default CategoryList;
